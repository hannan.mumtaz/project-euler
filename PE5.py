import numpy as np

def gcd(a,b):
  
  if a < b:
    a, b = b, a

  # base cases
  if a == 0 and b != 0:
    return b 
  elif b == 0 and a != 0:
    return a 
  elif a == b:
    return a
  
  while b > 0: 
    r = a%b 
    if r == 0:
      return b
    else:
      #print(a,b,r)
      a, b = b, r

  return b

#print(gcd(55,34))

def lcm(a,b):

  result = int(np.absolute(a*b) / gcd(a,b))

  return result 

#print(lcm(16,20))

def smallestMultiple(n):

  factors = list(range(2, n+1))
  for i in factors:
    for j in factors: 
      while factors[0] != factors[-1] and factors[0] != n:
        print(factors)
        c = lcm(factors[0], factors[-1])
        del factors[0]
        if c not in factors:
          factors.append(c)
      

  return (f"\nSmallest multiple: {max(factors)}")

print(smallestMultiple(10))
#print(smallestMultiple(20))
