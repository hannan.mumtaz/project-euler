def fiboEvenSum(n):
    fst = 0
    snd = 1
    next = 0
    sum = 0
  
    for i in range(n):
      next = fst + snd 
      fst = snd 
      snd = next 
      if next % 2 == 0:
        sum = sum + next
    

    return sum

print(fiboEvenSum(4000000))
    
#for treg