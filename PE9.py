# Special Pythagorean triplet, using Euclid's algorithm. 
def spt(sm): 

    ns = list(range(1, sm+1)) 

    for n in ns:
        for m in ns[n:]:        # condition: m > n > 0 
            a = m*m - n*n 
            b = 2*m*n 
            c = m*m + n*n 
            if a+b+c == 1000:
                result = a*b*c

    return result

print(spt(1000))


