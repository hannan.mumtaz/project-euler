import numpy as np

def primalityTest(n):
  if n < 2:
    return False
  else: 
    threshold = int(np.sqrt(n))
    for i in range(2,threshold+1):
      if n % i == 0:
        return False
      
  return True



def largestPrimeFactor(n):
  factors = []
  for i in range(2,n+1):
    if n%i == 0 and primalityTest(i):
      factors.append(i)

  return max(factors)

print(largestPrimeFactor(13195))

# correct, but too slow. 