import time
import numpy as np 

def primalityTest(n):
  if n < 2:
    return False
  else: 
    threshold = int(np.sqrt(n))
    for i in range(2,threshold+1):
      if n % i == 0:
        return False
      
  return True


def uniquePrimeFactors(n):
  factors = []
  for i in range(1,n+1):
    if n%i == 0 and primalityTest(i):
      factors.append(i)

  return factors 


def nthPrimes(l):
  primes = []
  i = 2
  while len(primes) < l:
    prime_factors =  uniquePrimeFactors(i)
    for n in prime_factors:
      if n not in primes:
        primes.append(n)

    i += 1
 
  return max(primes)

start = time.time()
print(nthPrimes(1000))
end = time.time()
print(end - start)

# took 178.63 seconds to find the answer for the 10001st prime being 104743. 
# should try to implement a better algorithm for the primality test.