
def sumOfSquares(n):
    sum = 0

    for i in range(n+1):
        sum += i**2

    return sum 
        

print(sumOfSquares(10))

def squareOfSum(n):
    sum = 0

    for i in range(n+1):
        sum += i 
    
    return sum ** 2

print(squareOfSum(10))

def difference_sqs_ssq(n):
    
    return squareOfSum(n) - sumOfSquares(n)

print(difference_sqs_ssq(10))
print(difference_sqs_ssq(100))