
import numpy as np 

def primalityTest(n):
  if n < 2:
    return False
  else: 
    threshold = int(np.sqrt(n))
    for i in range(2,threshold+1):
      if n % i == 0:
        return False
      
  return True


def summationOfPrimes(n):
  primes = []

  for i in range(1,n+1):
    if primalityTest(i):
      primes.append(i)

  return sum(primes) 

print(summationOfPrimes(2000000))